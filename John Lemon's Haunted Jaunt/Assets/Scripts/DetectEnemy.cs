﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //checks if there is a thief in their line of sight
    public class DetectEnemy : MonoBehaviour
    {
        private bool visionBlockedByWall;
        private GameObject wall;
        private List<GameObject> walls = new List<GameObject>();
        //if there is a thief and not a wall infront of the thief the thief is revealled
        private void OnTriggerEnter(Collider other)
        {
            
            if(other.gameObject.name == "Thief")
            {
                if (IsThereAWallBetweenCharacterAndUnit(other.gameObject.transform.position, other.gameObject.transform) == false)
                {
                    if (other.gameObject.GetComponent<GridCharacter>().visible == false)
                    {
                        other.gameObject.GetComponent<GridCharacter>().SwitchVisibility();
                    }
                }
            }
        }

        //while the object is in the collider, if the object moves in front of the wall then then the object is revealled
        private void OnTriggerStay(Collider other)
        {
            if(other.gameObject.name == "Thief")
            {
                if (IsThereAWallBetweenCharacterAndUnit(other.gameObject.transform.position, other.gameObject.transform) == false)
                {
                    if (other.gameObject.GetComponent<GridCharacter>().visible == false)
                    {
                        other.gameObject.GetComponent<GridCharacter>().SwitchVisibility();
                    }
                }

                if (IsThereAWallBetweenCharacterAndUnit(other.gameObject.transform.position, other.gameObject.transform) == true)
                {
                    if (other.gameObject.GetComponent<GridCharacter>().visible == true)
                    {
                        other.gameObject.GetComponent<GridCharacter>().SwitchVisibility();
                    }
                }
                
            }
        }

        //if the thief leaves the collider then the thief goes invisible again
        private void OnTriggerExit(Collider other)
        {
            if(other.gameObject.name == "Thief")
            {
                if(other.gameObject.GetComponent<GridCharacter>().visible == true)
                {
                    other.gameObject.GetComponent<GridCharacter>().SwitchVisibility();
                }
            }
        }

        //checks wether the wall is infront of the thief
        private bool IsThereAWallBetweenCharacterAndUnit(Vector3 characterPosition, Transform characterTransform)
        {
            Vector3 unitPosition = gameObject.transform.parent.GetComponent<BoxCollider>().transform.position;
            Vector3 distance = (characterPosition - unitPosition).normalized;
            //distance.y = .5f;
            RaycastHit hit;
            
            
            if(Physics.Raycast(unitPosition, distance, out hit, 10))
            {
                if(hit.transform == characterTransform)
                {
                    return false;
                }
            }
            
            
            
            return true;
        }
    }
}

