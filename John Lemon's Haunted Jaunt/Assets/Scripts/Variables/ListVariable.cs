﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    [CreateAssetMenu(menuName = "Variables/List of Nodes")]
    public class ListVariable : ScriptableObject
    {
        
        public List<int> value;
        
    }

}
