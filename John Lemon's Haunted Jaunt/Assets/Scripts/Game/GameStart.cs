﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SA
{
    //this script is for the beginning canvas to tell the player how to play
    public class GameStart : MonoBehaviour
    {
        private int numOfText = 9;
        public GameObject text;
        private bool removeCanvas = false;
        public CanvasGroup canvas;
        private float m_Timer;
        public float fadeDuration = 1f;
        public CanvasGroup ui;
        public GameManager game;

        //turn the text to the next
        public void turnText()
        {
            switch (numOfText)
            {
                case 9:
                    text.GetComponent<Text>().text = "Your Job is to protect the assets that are on the white tables.";
                    break;
                
                case 8:
                    text.GetComponent<Text>().text = "You will be controlling a robot to remotely patrol your building.";
                    break;
                
                case 7:
                    text.GetComponent<Text>().text = "Your robot is a base model so you have to pay attention to its visual feedback as it will not alert you to any new visual data.";
                    break;
                
                case 6:
                    text.GetComponent<Text>().text = "You can move the robot around via clicking on the robot then the tiles, note there is a limited range per command.";
                    break;
                
                case 5:
                    text.GetComponent<Text>().text = "You can move around your view via the WASD keys and the QE keys for rotating, understand that this is a blueprint representation, you cannot see anything your robot cannot see.";
                    break;
                
                case 4:
                    text.GetComponent<Text>().text = "Please keep in mind that your view cannot change while the computer is relaying comands and processing visual data so position it well";
                    break;
                
                case 3:
                    text.GetComponent<Text>().text = "The assets are attached to a weight sensor so the blueprint will update if and when a thief takes one.";
                    break;
                
                case 2:
                    text.GetComponent<Text>().text = "If you find a thief, which will take the form of a robo-rodent, you can automatically apprehend them if and only if the robot is directly next to them.";
                    break;
                
                case 1:
                    text.GetComponent<Text>().text = "Thank you for choosing Turestile Industries for your security solution, click the button to enter the software.";
                    break;
                
                //once we hit the end remove this canvas, make the ui usable and start game
                default:
                    removeCanvas = true;
                    canvas.interactable = false;
                    ui.interactable = true;
                    ui.blocksRaycasts = true;
                    game.StartGame();
                    break;
            }

            numOfText = numOfText - 1;
        }

        private void Update()
        {
            if (removeCanvas == true)
            {
                RemoveCanvas();
            }
        }
        //function to remove this canvas
        private void RemoveCanvas()
        {
            m_Timer += Time.deltaTime;
            float fade = m_Timer / fadeDuration;
            canvas.alpha -= fade;
            if (canvas.alpha <= 0f)
            {
                removeCanvas = false;
                Transform transform = gameObject.transform;
                foreach (Transform child in transform)
                {
                    Destroy(child.gameObject);
                }
                Destroy(gameObject);
            }
        }
    }
}

