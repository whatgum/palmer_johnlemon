﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEnd : MonoBehaviour
{
    
    private string m_endingType = "";
    public float fadeDuration = 1f;
    public CanvasGroup canvas;
    private float m_Timer;
    public CanvasGroup ui;

    //sets the ending to a certain type
    public void SetEndingType(string ending)
    {
        m_endingType = ending;
    }

    //don't do anything until the ending has been decided
    private void Update()
    {
        if (m_endingType != "")
        {
            EndGame(m_endingType);
        }
    }

    //displays an ending based on the ending given
    private void EndGame(string ending)
    {
        switch (ending) { 
        case "Finished Turns":
        
            gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = "It is now 8:00AM, You succeded in protecting the assets";
            break;

        case "caught thief":

                gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = "You caught the thief and got to go home early";
                break;

        case "thief win":
                gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = "The thief stole the artifact and you failed to catch him";
                break;

        default:
                gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Unknown reason for ending";
                break;

        }
        ui.alpha = 0f;
        m_Timer += Time.deltaTime;
        canvas.alpha = m_Timer / fadeDuration;
        if (canvas.alpha >= 1f)
        {
            m_endingType = "";

        }
    }
}
