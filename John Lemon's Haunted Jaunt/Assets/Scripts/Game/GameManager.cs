﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{
    //manages the game state
    public class GameManager : MonoBehaviour
    {
        int turnIndex;
        
        public Turn[] turns;
        public GameObject endTurnButton;
        public GameEnd gameEnd;
        public GridManager gridManager;
        public float delta;

        private bool isInit;

        public PathfinderMaster pathFinderMaster;
        public LineRenderer pathViz;
        GameObject tileContainer;
        bool isPathfinding;
        public bool nextTurn = false;

        #region Init
        public void StartGame()
        {
            //initalizes the game
            gridManager.Init();
            InitStateManagers();
            PlaceUnits();
            isInit = true;
            Invoke("TurnTurnsOn", 1f);
        }
        //turns the end turn button on
        void TurnTurnsOn()
        {
            endTurnButton.transform.GetComponent<EndTurnButton>().SwitchButtonInteractability();
        }

        //initalizes the state managers
        void InitStateManagers()
        {
            foreach (Turn t in turns)
            {
                t.player.Init();

            }
        }

        //places the units on the grid
        void PlaceUnits()
        {
            GridCharacter[] units = GameObject.FindObjectsOfType<GridCharacter>();
            foreach (GridCharacter unit in units)
            {
                unit.OnInit();
                Node n = gridManager.GetNode(unit.transform.position);

                if (n != null)
                {
                    unit.transform.position = n.worldPosition;
                    n.character = unit;
                    unit.currentNode = n;
                }
            }
        }
        #endregion

        #region Pathfinding Calls 

        //gets a path to a node for a character
        public void PathfinderCall(Node targetNode, GridCharacter character)
        {
            //if we aren't already pathfinding pathfind
            if (!isPathfinding)
            {
                isPathfinding = true;

                Node startPosition = character.currentNode;
                Node target = targetNode;

                PathfinderMaster.singleton.RequestPathFind(character, startPosition, targetNode, PathFinderCallBack, gridManager);
                
            }
        }

        //clear the path of the current character
        public void ClearPath(StateManager states)
        {
            pathViz.positionCount = 0;
            if(states.currentCharacter != null)
            {
                states.currentCharacter.currentPath = null;
            }
        }

        //adds the positions from the path to the characters path
        void PathFinderCallBack(List<Node> path, GridCharacter character)
        {
            isPathfinding = false;
            if (path == null)
            {
                Debug.LogWarning("Path is not valid");
                return;
            }

            Vector3 offset = new Vector3(0, 1f, 0);
            List<Vector3> allowedPositions = new List<Vector3>();

            allowedPositions.Add(character.currentNode.worldPosition + offset);

            for (int i = 0; i < path.Count; i++)
            {

                if (i <= character.tilesPerTurn)
                {

                    allowedPositions.Add(path[i].worldPosition + offset);

                }

            }

            List<Node> pathThisTurn = new List<Node>();
            
            //keeps it so the character can only go for how much their tiles per turn dictates
            for (int i = 0; i < character.tilesPerTurn && i < path.Count; i++)
            {
                pathThisTurn.Add(path[i]);
            }

            pathViz.positionCount = pathThisTurn.Count + 1;

            //gives the character the path to follow
            character.LoadPath(pathThisTurn);
            
            //if it the character belongs to the player then show them the line of guidance
            if (character.owner.name == "Player 1")
            {
                pathViz.SetPositions(allowedPositions.ToArray());
            }

        }

        #endregion

        #region Turn Management
        //makes it nexturn
        public void NextTurn()
        {
            if (nextTurn == false)
            {
                nextTurn = true;
            }
            else
            {
                nextTurn = false;
            }
        }
        #endregion

        private void Update()
        {
            
            if (!isInit)
            {
                return;
            }

            delta = Time.deltaTime;

            //executes the turns
            if (turns[turnIndex].Execute(this))
            {
                turnIndex++;

                if (turnIndex > turns.Length - 1)
                {
                    turnIndex = 0;
                }
            }

        }

    }

}
