﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class Pathfinder 
    {
    
        GridManager gridManager;
        GridCharacter character;
        Node startPosition;
        Node endPosition;
        List<Node> targetPath;

        public volatile float timer;
        public volatile bool jobDone = false;

        public delegate void PathfindingComplete(List<Node> n, GridCharacter character);

        PathfindingComplete completeCallBack;

        //constructs the pathfinder
        public Pathfinder(GridCharacter character, Node startPosition, Node endPosition, PathfindingComplete callback, GridManager gridManager)
        {
            this.gridManager = gridManager;
            this.character = character;
            this.startPosition = startPosition;
            this.endPosition = endPosition;
            completeCallBack = callback;
        }
        
        //finds a path
        public void FindPath()
        {
            targetPath = FindPathActual();
            jobDone = true;
        }

        //complete the call back
        public void NotifyComplete()
        {
           if(completeCallBack != null)
           {
               completeCallBack(targetPath, character);
           }
        }

        //using A* find a path to a node
        List<Node> FindPathActual()
        {
            List<Node> foundPath = new List<Node>();
            List<Node> openSet = new List<Node>();
            HashSet<Node> closedSet = new HashSet<Node>();

            openSet.Add(startPosition);
            while (openSet.Count > 0)
            {
               
                Node currentNode = openSet[0];
                
                for (int i = 0; i < openSet.Count; i++)
                {
                    
                    if(openSet[i].fCost < currentNode.fCost || (openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost))
                    {
                        if (!currentNode.Equals(openSet[i]))
                        {
                            currentNode = openSet[i];
                        }
                    }
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                if (currentNode.Equals(endPosition))
                {
                    foundPath = RetracePath(startPosition, currentNode);
                    break;
                }

                foreach (Node neighbor in GetNeighbors(currentNode))
                {
                    if(!closedSet.Contains(neighbor))
                    {
                        float newMovementCostToNeighbor = currentNode.gCost + GetDistance(currentNode, neighbor);

                        if(newMovementCostToNeighbor < neighbor.gCost || !openSet.Contains(neighbor))
                        {
                            neighbor.gCost = newMovementCostToNeighbor;
                            neighbor.hCost = GetDistance(neighbor, endPosition);
                            neighbor.parentNode = currentNode;

                            if (!openSet.Contains(neighbor))
                            {
                                openSet.Add(neighbor);
                            }
                        }
                    }
                }
            }

            return foundPath;
        }

        //gets the distance between nodes
        int GetDistance(Node positionA, Node positionB)
        {
            int distanceX = Mathf.Abs(positionA.x - positionB.x);
            int distanceZ = Mathf.Abs(positionA.z - positionB.z);

            if (distanceX > distanceZ)
            {
                return 14 * distanceZ + 10 * (distanceX - distanceZ);
            }

            return 14 * distanceX + 10 * (distanceZ - distanceX);
        }

        //gets the neighbor of a node
        Node GetNeighbor(Node currentNode)
        {
            Node returnNode = null;
            if(currentNode != null)
            {
                if (currentNode.isWalkable)
                {
                    returnNode = currentNode;
                    //if this node has a cop and the path is being used by a thief remove it from use
                    if (currentNode.character != null)
                    {
                        
                        if(currentNode.character.type == "cop" && character.type == "thief")
                        {  
                            returnNode = null;
                        }
                    }
                    
                }
            }

           return returnNode;
        }

        //used only by thief, check for a shortcut to use
        Node GetPotentialShortcut(Node currentNode)
        {
            Node returnNode = null;
            if(currentNode != null)
            {
                if(currentNode.isTraversable)
                {
                    returnNode = currentNode;
                }
            }
            return returnNode;
        }
        

        //returns a node using the gridmanager x z method
        Node GetNode(int x, int z)
        {
            return gridManager.GetNode(x, z);
        }

        //gets all the neighbors of a node
        List<Node> GetNeighbors(Node currentNode)
        {
            List<Node> returnList = new List<Node>();
            
            //if the current node is a shortcut find the other side of the shortcut
            if (currentNode.isTraversable && character.type == "thief")
            {
                for (int x = -2; x <= 2; x++)
                {
                    for (int z = -2; z <= 2; z++)
                    {
                        if (z == 0 && x == 0)
                        {
                            continue;
                        }
                        int trueX = x + currentNode.x;
                        int trueZ = z + currentNode.z;

                        Node n = GetNode(trueX, trueZ);

                        Node newNode = GetPotentialShortcut(n);

                        if (newNode != null)
                        {
                            returnList.Add(newNode);
                        }

                    
                    }
                }
            }

            //find nodes
            for (int x = -1; x <= 1; x++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (z == 0 && x == 0)
                    {
                        continue;
                    }
                    int trueX = x + currentNode.x;
                    int trueZ = z + currentNode.z;

                    Node n = GetNode(trueX, trueZ);

                    Node newNode = GetNeighbor(n);

                    if (newNode != null)
                    {
                        returnList.Add(newNode);
                    }

                    
                }
            }

            return returnList;
        }

        //makes the path backwards for the character to traverse
        List<Node> RetracePath(Node start, Node end)
        {
            List<Node> path = new List<Node>();
            Node currentNode = endPosition;
            while (currentNode != start)
            {
                path.Add(currentNode);
                currentNode = currentNode.parentNode;
            }

            path.Reverse();
            return path;
        }

    }

}
