﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

namespace SA
{
    public class PathfinderMaster : MonoBehaviour
    {
        public static PathfinderMaster singleton;

        public List<Pathfinder> currentJobs = new List<Pathfinder>();
        List<Pathfinder> toDoJobs = new List<Pathfinder>();
        public int MaxJobs = 3;
        public float timerThreashold = 5f;

        private void Awake()
        {
            singleton = this;
        }

        public void Update()
        {   
            float delta = Time.deltaTime;
            int i = 0;
            while(i < currentJobs.Count)
            {
                if (currentJobs[i].jobDone)
                {
                    currentJobs[i].NotifyComplete();
                    currentJobs.RemoveAt(i);
                }

                else
                {
                    currentJobs[i].timer += delta;
                    if(currentJobs[i].timer > timerThreashold)
                    {
                        currentJobs[i].jobDone = true;
                    }
                    i++;
                }
            }

            if(toDoJobs.Count > 0 && currentJobs.Count < MaxJobs)
            {
                Pathfinder job = toDoJobs[0];
                toDoJobs.RemoveAt(0);
                currentJobs.Add(job);
                Thread jobThread = new Thread(job.FindPath);
                jobThread.Start();
            }
        }

        public void RequestPathFind(GridCharacter character, Node startPosition, Node targetPosition, Pathfinder.PathfindingComplete callback, GridManager gridManager)
        {
            Pathfinder newJob = new Pathfinder(character, startPosition, targetPosition, callback, gridManager);
            toDoJobs.Add(newJob);

        }
    }
}

