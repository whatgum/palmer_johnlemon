﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class GridManager : MonoBehaviour
    {

        #region Variables
        Node[,] grid;

        [SerializeField]
        float xzScale = 1.5f;

        [SerializeField]
        float yScale = 1.5f;

        [SerializeField]
        Vector3 minPos;

        int maxX;
        int maxZ;
        int maxY;

        public bool visualizeCollisions;

        List<Vector3> nodeViz = new List<Vector3>();
        public Vector3 extends = new Vector3(.8f, .8f, .8f);

        public int posX;
        public int posZ;

        #endregion

        public GameObject unit;
        public GameObject tileViz;
        public GameObject tileContainer;

        //initiliazaiton
        public void  Init()
        {

            ReadLevel();

        }

        //creates a grid based on the grid positions 
        void ReadLevel()
        {
            GridPosition[] gp = GameObject.FindObjectsOfType<GridPosition>();
            
            float minX = float.MaxValue;
            float maxX = float.MinValue;
            float minZ = minX;
            float maxZ = maxX;

            for (int i = 0; i < gp.Length; i++)
            {
                Transform t = gp[i].transform;


                #region Read Positions
                if (t.position.x < minX)
                {
                    
                    minX = t.position.x;

                }

                if (t.position.x > maxX)
                {
                    maxX = t.position.x;
                }

                if (t.position.z < minZ)
                {
                    minZ = t.position.z;

                }


                if (t.position.z > maxZ)
                {
                    maxZ = t.position.z;
                }
                #endregion
            
            }


            posX = Mathf.FloorToInt((maxX - minX) / xzScale);
            posZ = Mathf.FloorToInt((maxZ - minZ) / xzScale);

            minPos = Vector3.zero;
            minPos.x = minX;
            minPos.z = minZ;
            CreateGrid(posX, posZ);
        }

        //creates the nodes for the grid
        void CreateGrid(int posX, int posZ)
        {
            grid = new Node[posX, posZ];

            for (int x = 0; x < posX; x++)
            {
                for (int z = 0; z < posZ; z++)
                {
                    Node n = new Node();
                    n.x = x;
                    n.z = z;

                    Vector3 tp = minPos;
                    tp.x += x * xzScale;
                    tp.z += z * xzScale;
                    tp.y = yScale;
                    n.worldPosition = tp; 

                    Vector3 collisionPosition = tp;
                    
                    Collider[] overlapNode = Physics.OverlapBox(collisionPosition, extends /2, Quaternion.identity);

                    //checks if there is a grid object and if there is, adjust the node that has this object
                    if (overlapNode.Length > 0)
                    {
                        bool isWalkable = false;
                        bool isTraversable = false;
                        for (int i = 0; i < overlapNode.Length; i++)
                        {
                            GridObject obj = overlapNode[i].transform.GetComponentInChildren<GridObject>();

                            if(obj != null)
                            {

                                if(obj.isWalkable && n.obstacle == null)
                                {
                                    isWalkable = true;
                                    if(obj.isTraversable)
                                    {
                                        isTraversable = true;
                                    }
                                }
                                else
                                {
                                        isWalkable = false;
                                        isTraversable = false;
                                        n.obstacle = obj;
                                }

                            }
                        }

                        n.isWalkable = isWalkable;
                        n.isTraversable = isTraversable;
                    }

                    //create a tile for the node if it is walkable
                    if (n.isWalkable == true) 
                    {
                        
                        GameObject go = Instantiate(tileViz, n.worldPosition, Quaternion.identity) as GameObject;
                        n.tileViz = go;
                        go.transform.parent = tileContainer.transform;
                    }

                    //if there is no obstical then add it the visiualizer
                    if (n.obstacle == null)
                    {
                        nodeViz.Add(collisionPosition);
                    }

                    grid[x,z] = n;
                }
                
            }

        }

        //gets a node based of the vector positon of that node
        public Node GetNode(Vector3 wallPositon)
        {
            Vector3 position = wallPositon - minPos;
            int x = Mathf.RoundToInt(position.x / xzScale);
            int z = Mathf.RoundToInt(position.z / xzScale);

            return GetNode(x, z);
        }

        //gets a node based on its x and z position on the grid
        public Node GetNode(int x, int z)
        {
            if (x < 0 || x > posX - 1 || z < 0 || z > posZ -1)
            {
                return null;
            }

            return grid[x, z];
        }

        //draws the outsides of the grid
        void OnDrawGizmos()
        {

            if(visualizeCollisions)
            {
                Gizmos.color = Color.red;
                
                for (int i = 0; i < nodeViz.Count; i++)
                {
                    Gizmos.DrawWireCube(nodeViz[i], extends);
                }
            }


            

        }
    }
    
    
}
