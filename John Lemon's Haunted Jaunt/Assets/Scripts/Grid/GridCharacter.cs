﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{
    
    public class GridCharacter : MonoBehaviour, ISelectable, IDeselect, IHighlight, IDeHighlight, IDetectable
    {
        public GameObject goals;
        public GameObject goal;
        public PlayerHolder owner;
        public Material canMove;
        public Material cantMove;
        public bool spent;
        public int tilesPerTurn;
        public GameObject highlighter;
        public bool isSelected;
        public float moveSpeed;
        public bool visible;
        public float rotationSpeed;
        [HideInInspector]
        public Node currentNode;
        [HideInInspector]
        public List<Node> currentPath;
        private int tiles;
        public string type;
        public GameObject body;

        //gives a path to the character
        public void LoadPath(List<Node> path)
        {
            currentPath = path;
        }

        //intialize the character
        public void OnInit()
        {
            owner.RegisterCharacter(this);
            tiles = tilesPerTurn;
            if (type == "thief")
            {
                SwitchVisibility();
                goal = goals.transform.GetChild(0).gameObject;
            }
        }

        //switch the visibility of the character
        public void SwitchVisibility()
        {
            if(body.activeSelf == true)
            {
                body.SetActive(false);
                visible = false;
            }
            else
            {
                body.SetActive(true);
                visible = true;
            }
        }

        //when the character is deselected
        public void OnDeselect(PlayerHolder player)
        {
            highlighter.SetActive(false);
            isSelected = false;
        }

        //when the character is selected
        public void OnSelect(PlayerHolder player)
        {
            highlighter.SetActive(true);
            isSelected = true;
            player.stateManager.currentCharacter = this;
        }

        //when the character is highlighted
        public void OnHighlight(PlayerHolder player)
        {
            highlighter.SetActive(true);
            
        }

        //when the character is dehighighlighted
        public void OnDeHighlight(PlayerHolder player)
        {
            if(!isSelected)
            {
                highlighter.SetActive(false);
                
            }
        }

        //resets the character for new turn
        public void ResetForNextTurn()
        {
            spent = false;
            tilesPerTurn = tiles;
            UpdateHighlighter();
        }

        //when character is detected
        public Node OnDetect()
        {
            return currentNode;
        }
        
        //changes the target destination of the character
        public void ChangeGoal()
        {
            goal.transform.parent = null;
            Destroy(goal);
            goal = goals.transform.GetChild(0).gameObject;
        }

        //updates the highliter based on wether they are spent or not
        public void UpdateHighlighter()
        {
            GameObject obj = this.gameObject.transform.GetChild(1).gameObject;
            Renderer objRenderer = obj.gameObject.transform.GetComponent<Renderer>() as Renderer;
            if (spent == true)
            {
                objRenderer.material = cantMove;
            }
            else
            {
                objRenderer.material = canMove;
            }
        }

    }


}

