﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //an abstract class to be used to manage each players states
    public abstract class StateManager : MonoBehaviour
    {
        public State currentState;
        public State startingState;
        public bool forceExit;

        public Node currentNode;
        public Node prevNode;
        public float delta;

        protected Dictionary<string, State> allStates = new Dictionary<string, State>();

        public PlayerHolder playerHolder;
        public GridCharacter currentCharacter{
            get{
                return _currentCharacter;
            }
            set 
            {  
                if (_currentCharacter != null)
                {
                    _currentCharacter.OnDeselect(playerHolder);
                }
                
                _currentCharacter = value;
                
            }
        }
        GridCharacter  _currentCharacter;
        
        private void Start()
        {
            Init();
        }

        public abstract void Init();
       

        public void Tick(GameManager gm, Turn turn)
        {   
            delta = gm.delta;

            if (currentState != null)
            {
                currentState.Tick(this, gm, turn);
            }

            forceExit = false;
        }

        public void SetState(string id)
        {
            State targetState = GetState(id);
            if (targetState == null)
            {
                Debug.LogError("State with id : " + id + " cannot be found! Check your states and IDs");
            }

            Debug.Log("Changed state to : " + id);
            currentState = targetState;

        }

        public void SetStateToStartingState()
        {
            currentState = startingState;
        }

        State GetState(string id)
        {
            State result = null;
            allStates.TryGetValue(id, out result);
            return result;
        }
    }

}

    
