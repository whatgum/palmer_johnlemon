﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{
    //the player state manager
    public class PlayerStateManager : StateManager
    {
        public override void Init()
        {
            VariablesHolder gameVariables = Resources.Load("GameVariables") as VariablesHolder;
            
            State interactions = new State();
            interactions.actions.Add(new InputManager(gameVariables));
            interactions.actions.Add(new HandleMouseInteractions());
            interactions.actions.Add(new MoveCameraTransform(gameVariables));
            State wait = new State();
            State checkForThief = new State();
            checkForThief.actions.Add(new CheckForThief());

            State moveOnPath = new State();           
            moveOnPath.actions.Add(new MoveCharacterOnPath());
 
            State endTurn = new State();
            endTurn.actions.Add(new EndTurn());

            currentState = interactions;
            startingState = interactions;
            
            allStates.Add("interactions", interactions);
            allStates.Add("wait", wait);
            allStates.Add("moveOnPath", moveOnPath);
            allStates.Add("endTurn", endTurn);
            allStates.Add("checkForThief", checkForThief);

        }

    }
}

