﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //the enemy ai state manager
    public class EnemyStateManager : StateManager
    {
        public override void Init()
        {
            
            State selectEnemy = new State();
            selectEnemy.actions.Add(new SelectEnemy());

            State moveEnemy = new State();
            moveEnemy.actions.Add(new MoveEnemyTowardGoal());
            
            State moveOnPath = new State();
            moveOnPath.actions.Add(new MoveCharacterOnPath());
            
            State endTurn = new State();
            endTurn.actions.Add(new EndTurn());

            State endGame = new State();
            endGame.actions.Add(new ThiefEndGame());

            State wait = new State();

            currentState = selectEnemy;
            startingState = selectEnemy;

            allStates.Add("giveGoal", moveEnemy);
            allStates.Add("selectEnemy", selectEnemy);
            allStates.Add("moveOnPath", moveOnPath);
            allStates.Add("endTurn", endTurn);
            allStates.Add("endGame", endGame);
            allStates.Add("wait", wait);

        }
    }

}


