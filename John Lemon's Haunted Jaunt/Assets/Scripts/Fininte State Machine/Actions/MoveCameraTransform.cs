﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //a state action that moves and rotates the camera
    public class MoveCameraTransform : StateActions
    {
        
        TransformVariable cameraTransform;
        FloatVariable horizontal;
        FloatVariable vertical;
        FloatVariable rotation;
        VariablesHolder varHolder;

        //a constructor that initializes the variables
        public MoveCameraTransform(VariablesHolder holder)
        {
            varHolder = holder;
            cameraTransform = varHolder.cameraTransform;
            horizontal = varHolder.horizontalInput;
            vertical = varHolder.verticalInput;
            rotation = varHolder.rotationInput;
        }

        
        public override void Execute(StateManager states, GameManager gm, Turn turn)
        {
            Vector3 targetPosition = cameraTransform.value.forward * (vertical.value * varHolder.cameraMoveSpeed * states.delta);
            targetPosition += cameraTransform.value.right * (horizontal.value * varHolder.cameraMoveSpeed * states.delta);
            cameraTransform.value.position += targetPosition;
            
            cameraTransform.value.Rotate(new Vector3(0, rotation.value, 0f));
            
            
        }

    }

}
