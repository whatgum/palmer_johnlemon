﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //a state that checks to see if a thief is near a cop 
    public class CheckForThief : StateActions
    {
        public GameEnd ending = GameObject.FindGameObjectWithTag("canvas").GetComponent<GameEnd>();

        public override void Execute(StateManager states, GameManager gm, Turn turn)
        {
            GridCharacter character = states.currentCharacter;
            Node currentNode = character.currentNode;

            //check the nodes that are above, below and beside current node
            for (int x = -1; x <= 1; x++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if ((z == 0 && x == 0) || (z != 0 && x != 0))
                    {
                        continue;
                    }
                    int trueX = x + currentNode.x;
                    int trueZ = z + currentNode.z;

                    Node n = gm.gridManager.GetNode(trueX, trueZ);

                    //if theres a theif end the game
                    if (n != null)
                    {
                        if(n.character != null)
                        {
                            if (n.character.type == "thief")
                            {  
                                Debug.Log(x + " " + z);
                                ending.SetEndingType("caught thief");
                            }
                        }
                        
                    }


                }
            }

            


            states.SetStateToStartingState();
        }
    }

}

