﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //a state that ends the turn
    public class EndTurn : StateActions 
    {
        public override void Execute(StateManager states, GameManager gm, Turn turn)
        {
            List<GridCharacter> listOfCharacters = states.playerHolder.characters;

            //for every character the player has reset them
            foreach (GridCharacter character in listOfCharacters)
            {
                character.ResetForNextTurn();
                if(character.type == "thief")
                {
                    if(character.currentNode == gm.gridManager.GetNode(character.goal.transform.position))
                    {
                        character.ChangeGoal();
                    }
                }
            }
            
            GameObject turnText = GameObject.FindGameObjectWithTag("turnText");
            
            //if its the ai ending turn make it so player one can play
            if(states.playerHolder.name == "Player 2")
            {
                GameObject button = GameObject.FindGameObjectWithTag("Finish");
                
                button.transform.GetComponent<EndTurnButton>().SwitchButtonInteractability();
                button.transform.GetComponent<EndTurnButton>().NewTurn();
                turnText.GetComponent<OpponentTurnText>().OpponentTurn(false);
            }

            //if its the player ending turn make it so player cannot play
            if(states.playerHolder.name == "Player 1")
            {
                turnText.GetComponent<OpponentTurnText>().OpponentTurn(true);
            }
            
            gm.NextTurn();
            states.SetState("wait");
        }
    }
}

