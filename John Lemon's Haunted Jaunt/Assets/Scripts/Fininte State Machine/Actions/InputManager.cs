﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //a state action that keeps track of inputs besides mouse
    public class InputManager : StateActions
    {
        VariablesHolder varHolder;
        public InputManager(VariablesHolder holder)
        {
             varHolder = holder;
             
        }
        public override void Execute(StateManager states, GameManager gm, Turn turns)
        {
            varHolder.rotationInput.value = 0;
            varHolder.horizontalInput.value = Input.GetAxis("Horizontal");
            varHolder.verticalInput.value = Input.GetAxis("Vertical");
            if (Input.GetButton("Rotate Camera Left"))
            {
                varHolder.rotationInput.value = -1f;

            }

            if (Input.GetButton("Rotate Camera Right"))
            {
                varHolder.rotationInput.value = 1f;
            }
            
        }
    }

}

