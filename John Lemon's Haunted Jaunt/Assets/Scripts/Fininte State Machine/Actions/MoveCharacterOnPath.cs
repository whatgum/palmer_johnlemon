﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //a state that makes the character move to their new position
    public class MoveCharacterOnPath : StateActions
    {
        bool isInit;

        float t;
        float rotationT;
        float speed;
        float rotationSpeed;

        Node startPosition;
        Node targetNode;

        int index;

        Quaternion targetRotation;
        Quaternion StartingRotation;

        public override void Execute(StateManager states, GameManager gm, Turn turn)
        {
            GridCharacter character = states.currentCharacter;
            
            //if it hasn't been initialized
            if (!isInit)
            {
                //make sure that it has everything it needs otherwise return to starting state
                if(character == null || character.currentPath == null || index > character.currentPath.Count -1)
                {
                    
                    states.SetStateToStartingState();
                    return;         
                }

                isInit = true;
                startPosition = character.currentNode;
                targetNode = character.currentPath[index];

                float newT = t - 1;
                newT = Mathf.Clamp01(newT);
                t = newT;
                float distance = Vector3.Distance(startPosition.worldPosition, targetNode.worldPosition);
                speed = character.moveSpeed /distance;
                rotationSpeed = character.rotationSpeed / distance;

                Vector3 direction = targetNode.worldPosition - startPosition.worldPosition;
                targetRotation = Quaternion.LookRotation(direction);
                StartingRotation = character.transform.rotation;
                
            }

            t += states.delta * speed;
            rotationT +=  states.delta * rotationSpeed;
            
            if (rotationT > 1)
            {

                rotationT = 1;
            }


            if (t > 1)
            {
                isInit = false;
                character.currentNode.character = null;
                character.currentNode = targetNode;
                character.currentNode.character = character;
                
                index++;
                
                //if the character is at their destintation
                if(index > character.currentPath.Count -1)
                {
                    t = 1;
                    index = 0;
                    //reduce the number of tiles they can walk
                    character.tilesPerTurn = character.tilesPerTurn - character.currentPath.Count;
                    
                    //if they can walk no more update their variables to reflect that
                    if (character.tilesPerTurn  <= 0)
                    {   
                        
                        character.spent = true;
                        character.UpdateHighlighter();
                        character.currentPath = null;
                    }

                    //if the character is a cop then check for a theif 
                    if (character.type == "cop")
                    {
                        states.SetState("checkForThief");
                        return;
                    }
                    states.SetStateToStartingState();

                }
            }
            
            Vector3 targetPosition = Vector3.Lerp(startPosition.worldPosition, targetNode.worldPosition, t);

            character.transform.rotation = Quaternion.Slerp(StartingRotation, targetRotation, rotationT);
            character.transform.position = targetPosition;
        }
    }
}
