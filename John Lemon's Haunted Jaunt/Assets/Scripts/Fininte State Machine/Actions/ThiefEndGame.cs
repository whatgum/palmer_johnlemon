﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //a simple state that ends the game with the thief winning
    public class ThiefEndGame : StateActions
    {
        public override void Execute(StateManager states, GameManager gm, Turn turn)
        {
            gm.gameEnd.SetEndingType("thief win");
        }

    }
}

