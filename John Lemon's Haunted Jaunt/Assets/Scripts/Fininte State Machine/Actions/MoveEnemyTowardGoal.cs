﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //a state that gives an enemy a goal position to go to
    public class MoveEnemyTowardGoal : StateActions
    {
        public override void Execute(StateManager states, GameManager gm, Turn turn)
        {
            GridCharacter character = states.currentCharacter;
            GameObject goal = character.goal;
            
            Node n = gm.gridManager.GetNode(goal.transform.position);
            
            //if the thief made it to the end of their route end the game
            if(goal.name == "thiefExit" && character.currentNode == n)
            {
                states.SetState("endGame");
                return;
            }

            //if the character has a path to go on, make the character move on it
            if(character.currentPath != null)
            {

                if (character.currentPath.Count > 0)
                {

                    states.SetState("moveOnPath");
                    
                }

                else
                {
                    PathDetection(states, gm, n);
                }
            }

            else
            {
                PathDetection(states, gm, n);
            }
            
        }

        //finds a path toward the goal for the character
        void PathDetection(StateManager states, GameManager gm, Node node)
        {
            states.currentNode = node;

            if (states.currentNode != null)
            {
                if (states.currentNode != states.prevNode || states.prevNode == null)
                {
                    
                    gm.PathfinderCall(states.currentNode, states.currentCharacter);
                    gm.ClearPath(states);

                }
            }

            states.SetStateToStartingState();

        }

    }
}

