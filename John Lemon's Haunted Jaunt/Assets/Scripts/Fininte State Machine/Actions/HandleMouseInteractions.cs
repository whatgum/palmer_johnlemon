﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SA
{
    //A state action that handles mouse interactions
    public class HandleMouseInteractions : StateActions
    {
        GridCharacter previousCharacter;

        public override void Execute(StateManager states, GameManager gm, Turn turn)
        {
            GameObject button = gm.endTurnButton;
            bool mouseClick = Input.GetMouseButtonDown(0);

            //if their is a current object selectied
            if(EventSystem.current.currentSelectedGameObject != null)
            {  
                //if that object is the end turn button
                if(EventSystem.current.currentSelectedGameObject.name == button.name)
                {
                    //end turn
                    states.SetState("endTurn");
                    button.transform.GetComponent<EndTurnButton>().SwitchButtonInteractability();
                    EventSystem.current.SetSelectedGameObject(null);
                }
                else
                {
                    MouseHandling(states,  gm, turn, mouseClick);
                }
                
            }
            //otherwise figure out what the player is doing
            else
            {
                
                MouseHandling(states,  gm, turn, mouseClick);
                
            }
        }

        void MouseHandling(StateManager states, GameManager gm, Turn turn, bool mouseClick)
        {
            //if there is a previous character and it isnt the current character dehighlight that character
            if(previousCharacter != null && previousCharacter != states.currentCharacter)
            {
                previousCharacter.OnDeHighlight(states.playerHolder);
            }

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            //if the click is on a object in the game
            if (Physics.Raycast(ray, out hit, 1000))
            {
                //get a node from that object
                Node node = gm.gridManager.GetNode(hit.point);

                IDetectable detectable = hit.transform.GetComponent<IDetectable>();
                if(detectable != null)
                {
                    node = detectable.OnDetect();
                }
                //if their is a node
                if (node != null)
                {
                    //if there is a character on that node
                    if(node.character != null)
                    {
                        //if the character is owned by the player highlight them
                        if (node.character.owner == states.playerHolder)
                        {
                            node.character.OnHighlight(states.playerHolder);
                            previousCharacter = node.character;
                            gm.ClearPath(states);
                        }
                    }

                    //if there is a current highlighted charachter which isn't spent and the node doesn't have a character on it
                    if (states.currentCharacter != null && node.character == null && states.currentCharacter.spent == false && states.currentCharacter.highlighter.activeSelf == true)
                    {
                        //if the person clicked the mouse
                        if (mouseClick)
                        {
                            //and the character has a path to go on and the path to go on is above 0 move them
                            if (states.currentCharacter.currentPath != null && states.currentCharacter.currentPath.Count > 0)
                            {
                                states.SetState("moveOnPath");
                            }
                        }

                        //otherwise find a path
                        else
                        {
                            PathDetection(states, gm , node);
                        }
                    }

                    else
                    {
                        
                        if(mouseClick)
                        {    
                            //if there is a character on that node
                            if(node.character != null)
                            {
                                //and the player owns them, select them
                                if (node.character.owner == states.playerHolder)
                                {
                                    node.character.OnSelect(states.playerHolder);
                                    states.prevNode = null;
                                    gm.ClearPath(states);
                                }
                            }

                        }
                    }
                }
            }
        }

        //finds a path
        void PathDetection(StateManager states, GameManager gm, Node node)
        {
            states.currentNode = node;

            //if the goal exists
            if (states.currentNode != null)
            {   
                //and it isn't the last goal
                if (states.currentNode != states.prevNode || states.prevNode == null)
                {
                    states.prevNode = states.currentNode;
                    gm.PathfinderCall(states.currentNode, states.currentCharacter);
                    gm.ClearPath(states);
                }
            }
        }

       

    }

}

