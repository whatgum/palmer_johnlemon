﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //A state that selects enemies in order, moves them then ends turn
    public class SelectEnemy : StateActions
    {
        public override void Execute(StateManager states, GameManager gm, Turn turn)
        {
            PlayerHolder player = states.playerHolder;
            int numSpent = player.characters.Count;

            //for every enemey: move them
            foreach (GridCharacter character in player.characters)
            {
                //if they aren't already spent that turn
                if (character.spent != true)
                {
                    states.currentCharacter = character;
                    states.SetState("giveGoal");
                }
                else
                {
                    numSpent -= 1;
                }
                
            }
            //if there are no more unspent enemies
            if(numSpent == 0)
            {
                states.SetState("endTurn");
            }

        }

    }

}
