﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{
    //an abstract class to create state actions
    public abstract class StateActions 
    {
        public abstract void Execute(StateManager states, GameManager gm, Turn turn);
    }
}
