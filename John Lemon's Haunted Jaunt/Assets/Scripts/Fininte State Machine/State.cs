﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    //a class that is used to execute state actions attached to it
    public class State 
    {
        public List<StateActions> actions = new List<StateActions>();

        public void Tick(StateManager states, GameManager gm, Turn turn)
        {
            if (states.forceExit)
            {
                return;
            }
        
            for (int i = 0; i < actions.Count; i++)
            {
               actions[i].Execute(states, gm, turn);
               
            }

        }
    }
}

