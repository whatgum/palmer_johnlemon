﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EndTurnButton : MonoBehaviour
{
    public TurnText turn;
    public GameEnd ending;
    
    //end the turn unless we are on last turn then end game
    public void NewTurn()
    {
        if (turn.IncreaseTurn() == false)
        {
            ending.SetEndingType("Finished Turns");
            SwitchButtonInteractability();
        }
        
    }

    //switch button interactiablity
    public void SwitchButtonInteractability()
    {
        if (gameObject.GetComponent<Button>().interactable == false)
        {
            gameObject.GetComponent<Button>().interactable = true;
        }

        else
        {
            gameObject.GetComponent<Button>().interactable = false;
        }
    }
}
