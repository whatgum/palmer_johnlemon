﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TurnText : MonoBehaviour
{
    private int m_turn = 1;
    // Start is called before the first frame update
    public int m_turnsTillOver = 10;

    //increases the turn and returns wether the game is over
    public bool IncreaseTurn()
    {
       return UpdateTurn();
    }

    //updates the turn coutnter and returns wether game is over
    private bool UpdateTurn()
    {
        m_turn++;
        if (m_turn > m_turnsTillOver)
        {
            return false;
        }

        else
        {

            gameObject.GetComponent<Text>().text = "Turn: " + m_turn.ToString() + " / " + m_turnsTillOver.ToString();
            return true;
        }
    }
}
