﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OpponentTurnText : MonoBehaviour
{
    
    private int m_textTimer = 0;
    public int timeIntervals = 10;
    private bool opponentTurn = false;

    //makes a load text for the player to look at while they wait for enemy turn to end
    void Update()
    {
        //if the opponent is playing set the text to the load text and simulate loading
        if (opponentTurn == true)
        {
            if (gameObject.GetComponent<Text>().text == "")
            {
                gameObject.GetComponent<Text>().text = "Thieves' Turn.";
            }
            else
            {
                LoadingText();
            }

        }
        //otherwise hide text
        else
        {
            gameObject.GetComponent<Text>().text = "";
        }

    }

    //changes the bool to true or false
    public void OpponentTurn(bool isOpponentTurn)
    {
        if(isOpponentTurn == true)
        {
            opponentTurn = true;
        }
        else
        {
           opponentTurn = false;
        }
    }

    //simulates loading the text by changing the text ever short while
    private void LoadingText()
    {
        m_textTimer++;

        //if the timer has exceeded the timer intervals change the text
        if (m_textTimer > timeIntervals)
        {
            m_textTimer = 0;
            string text = gameObject.GetComponent<Text>().text;
            if (text.Length == 16)
            {
                text =text.Substring(0, 14);
            }
            else
            {
                text = text + ".";
                
            }
            gameObject.GetComponent<Text>().text = text;

        }

    }

}
