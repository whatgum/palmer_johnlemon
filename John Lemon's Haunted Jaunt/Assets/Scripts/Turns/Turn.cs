﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{

    [CreateAssetMenu(menuName = "Game/Turn")]
    public class Turn : ScriptableObject
    {   
        [System.NonSerialized]
        int index = 0;

        public PlayerHolder player;

        public Phase[] phases;

        private bool startPhase = true;

        //keeps a turn on a phase until that phase is done and when all phases are done move to next turn
        public bool Execute(GameManager gm)
        {
            bool result = false;

            if (startPhase == true)
            {
                phases[index].OnStartPhase(gm, this);
                startPhase = false;
            }

            if (phases[index].IsComplete(gm, this))
            {
                phases[index].OnEndPhase(gm, this);
                index++;
                startPhase = true;
                if (index > phases.Length - 1)
                {
                    index = 0;
                    result = true;
                }
            }

            return result;
        }


        public void EndCurrentPhase()
        {
            phases[index].forceExit = true;
        }
    }

}
