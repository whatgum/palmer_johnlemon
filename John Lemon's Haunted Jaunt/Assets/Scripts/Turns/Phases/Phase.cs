﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{
    //an abstract class that has the basis for all phases
    public abstract class Phase : ScriptableObject
    {
        public string phaseName;

        public bool forceExit;

        //checks if phase is over
        public abstract bool IsComplete(GameManager gm, Turn turn);

        [System.NonSerialized]
        protected bool isInit;

        //what happens when phase starts
        public abstract void OnStartPhase(GameManager gm, Turn turn);

        //what happesn when phase is over
        public virtual void OnEndPhase(GameManager gm, Turn turn)
        {
            isInit = false;
            forceExit = false;
        }
    }

}
