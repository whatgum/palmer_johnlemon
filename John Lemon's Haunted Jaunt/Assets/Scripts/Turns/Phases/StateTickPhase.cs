﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    
    [CreateAssetMenu(menuName = "Phases/States Tick")]
    public class StateTickPhase : Phase
    {
        public override bool IsComplete(GameManager gm, Turn turn)
        {
            turn.player.stateManager.Tick(gm, turn);
            if (gm.nextTurn == true)
            {
                return true;
            }
            return false;
        }
        //makes the next turn variable false and sets the state of the current player to the starting state
        public override void OnStartPhase(GameManager gm, Turn turn)
        {
            gm.NextTurn();
            turn.player.stateManager.SetStateToStartingState();
        }
        
         public override void OnEndPhase(GameManager gm, Turn turn)
        {
           Debug.Log("phase over");
        }
    }

}
 
