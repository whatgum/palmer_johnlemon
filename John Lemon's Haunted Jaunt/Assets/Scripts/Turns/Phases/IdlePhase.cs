﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{

    [CreateAssetMenu(menuName = "Phases/Idle Phase")]
    public class IdlePhase : Phase
    {
        public override bool IsComplete(GameManager gm, Turn turn)
        {
            return true;
        }

        public override void OnStartPhase(GameManager gm, Turn turn)
        {
             if(isInit)
            {
               return;
            }
            
            isInit = true;
            
            Debug.Log("Idle Phase");

        }

        public override void OnEndPhase(GameManager gm, Turn turn)
        {

        }
    }
}

