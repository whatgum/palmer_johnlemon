﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{
    [CreateAssetMenu(menuName = "Game/Player Holder")]
    public class PlayerHolder : ScriptableObject
    {

        [System.NonSerialized]
        public StateManager stateManager;

        [System.NonSerialized]
        GameObject stateManagerObject;

        public GameObject stateManagerPrefab;

        [System.NonSerialized]
        public List<GridCharacter> characters = new List<GridCharacter>();

        //initilizes the holder
        public void Init()
        {
            stateManagerObject = Instantiate(stateManagerPrefab) as GameObject;
            stateManager = stateManagerObject.GetComponent<StateManager>();
            stateManager.playerHolder = this;
            characters.Clear();
        }

        //adds all its characters to itself
        public void RegisterCharacter(GridCharacter character)
        {
            
            characters.Add(character);
            
        }

        //removes a character from itself
        public void UnRegisterCharacter(GridCharacter character)
        {
            if (characters.Contains(character))
            {
                characters.Remove(character);
            }
        }
    }

}
