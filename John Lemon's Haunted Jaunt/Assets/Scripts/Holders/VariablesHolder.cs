﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    [CreateAssetMenu(menuName ="Game Variables Holder")]
    //holds variables
    public class VariablesHolder : ScriptableObject
    {   
        public float cameraMoveSpeed = 15;

        [Header("Scriptable Variables")]
        #region Scriptables
        public TransformVariable cameraTransform;
        public FloatVariable horizontalInput;
        public FloatVariable verticalInput;
        public FloatVariable rotationInput;
        
        public ListVariable listOfNodes;
        #endregion

    }

}
