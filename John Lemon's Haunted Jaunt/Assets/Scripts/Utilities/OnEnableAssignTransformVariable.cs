﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class OnEnableAssignTransformVariable : MonoBehaviour
    {
        public TransformVariable targetVariable;

        void Awake()
        {
        
            targetVariable.value = this.transform;
            Destroy(this);
        
        }
    }

}
